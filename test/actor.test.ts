import {ChiselSnsConfiguration, awsActor, ChiselShardedSnsConfiguration} from '../src'
import {InternalTriggeredEvent} from '@othree.io/chisel'
import {PublishCommand} from '@aws-sdk/client-sns'
import {hashCode} from '../src/actor'

describe('hashCode', () => {
    it('should return the values hash code', () => {
        const result = hashCode('value')

        expect(result).toEqual(111972721)
    })
})

describe('publishEvent', () => {
    interface ExampleAggregateRoot {
        readonly message: string
    }

    const configuration: ChiselSnsConfiguration<ExampleAggregateRoot> = {
        IsFifo: false,
        TopicArn: 'aws:arn:Topic',
        BoundedContext: 'ExampleBC'
    }

    it('should return true if successful', async () => {
        const event: InternalTriggeredEvent = {
            contextId: '1337',
            eventId: '001',
            eventDate: 9000,
            type: 'SomethingCreated',
            body: {
                message: 'Awesome message'
            },
            metadata: {
                user: 'chuck.norris'
            }
        }

        const state: ExampleAggregateRoot = {
            message: 'Awesome message'
        }

        const snsClient = {
            send: jest.fn().mockResolvedValue({ MessageId: 'm3554g3'})
        } as any

        const maybeSuccess = await awsActor.publishEvent(snsClient, configuration)(event, state)

        expect(maybeSuccess.isPresent).toBeTruthy()
        expect(maybeSuccess.get()).toBeTruthy()

        expect(snsClient.send).toBeCalledTimes(1)
        expect(snsClient.send.mock.calls[0][0] instanceof PublishCommand).toBeTruthy()
        expect(snsClient.send.mock.calls[0][0].input).toStrictEqual({
            Message: JSON.stringify({
                event: event,
                state: state
            }),
            MessageAttributes: {
                user: {
                    DataType: 'String',
                    StringValue: 'chuck.norris'
                },
                bc: {
                    DataType: 'String',
                    StringValue: configuration.BoundedContext
                },
                eventType: {
                    DataType: 'String',
                    StringValue: event.type.toString()
                }
            },
            MessageGroupId: undefined,
            TopicArn: configuration.TopicArn
        })
    })

    it('should return true if successful when sending to a fifo sns', async () => {
        const fifoConfiguration: ChiselSnsConfiguration<ExampleAggregateRoot> = {
            ...configuration,
            IsFifo: true
        }

        const event: InternalTriggeredEvent = {
            contextId: '1337',
            eventId: '001',
            eventDate: 9000,
            type: 'SomethingCreated',
            body: {
                message: 'Awesome message'
            }
        }

        const state: ExampleAggregateRoot = {
            message: 'Awesome message'
        }

        const snsClient = {
            send: jest.fn().mockResolvedValue({ MessageId: 'm3554g3'})
        } as any

        const maybeSuccess = await awsActor.publishEvent(snsClient, fifoConfiguration)(event, state)

        expect(maybeSuccess.isPresent).toBeTruthy()
        expect(maybeSuccess.get()).toBeTruthy()

        expect(snsClient.send).toBeCalledTimes(1)
        expect(snsClient.send.mock.calls[0][0] instanceof PublishCommand).toBeTruthy()
        expect(snsClient.send.mock.calls[0][0].input).toStrictEqual({
            Message: JSON.stringify({
                event: event,
                state: state
            }),
            MessageAttributes: {
                bc: {
                    DataType: 'String',
                    StringValue: fifoConfiguration.BoundedContext
                },
                eventType: {
                    DataType: 'String',
                    StringValue: event.type.toString()
                }
            },
            MessageGroupId: '1337',
            TopicArn: fifoConfiguration.TopicArn
        })
    })

    it('should return true if successful when sending to a fifo sns and a custom MessageGroupId', async () => {
        const fifoConfiguration: ChiselSnsConfiguration<ExampleAggregateRoot> = {
            ...configuration,
            IsFifo: true,
            GetMessageGroupId: (event: InternalTriggeredEvent, state: ExampleAggregateRoot) => state.message
        }

        const event: InternalTriggeredEvent = {
            contextId: '1337',
            eventId: '001',
            eventDate: 9000,
            type: 'SomethingCreated',
            body: {
                message: 'Awesome message'
            }
        }

        const state: ExampleAggregateRoot = {
            message: 'Awesome message'
        }

        const snsClient = {
            send: jest.fn().mockResolvedValue({ MessageId: 'm3554g3'})
        } as any

        const maybeSuccess = await awsActor.publishEvent(snsClient, fifoConfiguration)(event, state)

        expect(maybeSuccess.isPresent).toBeTruthy()
        expect(maybeSuccess.get()).toBeTruthy()

        expect(snsClient.send).toBeCalledTimes(1)
        expect(snsClient.send.mock.calls[0][0] instanceof PublishCommand).toBeTruthy()
        expect(snsClient.send.mock.calls[0][0].input).toStrictEqual({
            Message: JSON.stringify({
                event: event,
                state: state
            }),
            MessageAttributes: {
                bc: {
                    DataType: 'String',
                    StringValue: fifoConfiguration.BoundedContext
                },
                eventType: {
                    DataType: 'String',
                    StringValue: event.type.toString()
                }
            },
            MessageGroupId: 'Awesome message',
            TopicArn: fifoConfiguration.TopicArn
        })
    })
})

describe('publishShardedEvent', () => {
    interface ExampleAggregateRoot {
        readonly message: string
    }

    const configuration: ChiselShardedSnsConfiguration<ExampleAggregateRoot> = {
        IsFifo: false,
        TopicsArn: ['aws:arn:Topic0', 'aws:arn:Topic1', 'aws:arn:Topic2'],
        BoundedContext: 'ExampleBC'
    }

    it('should return true if successful', async () => {
        const event: InternalTriggeredEvent = {
            contextId: '1337',
            eventId: '001',
            eventDate: 9000,
            type: 'SomethingCreated',
            body: {
                message: 'Awesome message'
            },
            metadata: {
                user: 'chuck.norris'
            }
        }

        const state: ExampleAggregateRoot = {
            message: 'Awesome message'
        }

        const snsClient = {
            send: jest.fn().mockResolvedValue({ MessageId: 'm3554g3'})
        } as any

        const hashCode = jest.fn().mockReturnValue(22)

        const maybeSuccess = await awsActor.publishShardedEvent(hashCode)(snsClient, configuration)(event, state)

        expect(maybeSuccess.isPresent).toBeTruthy()
        expect(maybeSuccess.get()).toBeTruthy()

        expect(hashCode).toBeCalledTimes(1)
        expect(hashCode).toBeCalledWith(event.contextId)

        expect(snsClient.send).toBeCalledTimes(1)
        expect(snsClient.send.mock.calls[0][0] instanceof PublishCommand).toBeTruthy()
        expect(snsClient.send.mock.calls[0][0].input).toStrictEqual({
            Message: JSON.stringify({
                event: event,
                state: state
            }),
            MessageAttributes: {
                user: {
                    DataType: 'String',
                    StringValue: 'chuck.norris'
                },
                bc: {
                    DataType: 'String',
                    StringValue: configuration.BoundedContext
                },
                eventType: {
                    DataType: 'String',
                    StringValue: event.type.toString()
                }
            },
            MessageGroupId: undefined,
            TopicArn: configuration.TopicsArn[1]
        })
    })

    it('should return true if successful when sending to a fifo sns', async () => {
        const fifoConfiguration: ChiselShardedSnsConfiguration<ExampleAggregateRoot> = {
            ...configuration,
            IsFifo: true
        }

        const event: InternalTriggeredEvent = {
            contextId: '1337',
            eventId: '001',
            eventDate: 9000,
            type: 'SomethingCreated',
            body: {
                message: 'Awesome message'
            }
        }

        const state: ExampleAggregateRoot = {
            message: 'Awesome message'
        }

        const snsClient = {
            send: jest.fn().mockResolvedValue({ MessageId: 'm3554g3'})
        } as any

        const hashCode = jest.fn().mockReturnValue(21)

        const maybeSuccess = await awsActor.publishShardedEvent(hashCode)(snsClient, fifoConfiguration)(event, state)

        expect(maybeSuccess.isPresent).toBeTruthy()
        expect(maybeSuccess.get()).toBeTruthy()

        expect(hashCode).toBeCalledTimes(1)
        expect(hashCode).toBeCalledWith(event.contextId)

        expect(snsClient.send).toBeCalledTimes(1)
        expect(snsClient.send.mock.calls[0][0] instanceof PublishCommand).toBeTruthy()
        expect(snsClient.send.mock.calls[0][0].input).toStrictEqual({
            Message: JSON.stringify({
                event: event,
                state: state
            }),
            MessageAttributes: {
                bc: {
                    DataType: 'String',
                    StringValue: fifoConfiguration.BoundedContext
                },
                eventType: {
                    DataType: 'String',
                    StringValue: event.type.toString()
                }
            },
            MessageGroupId: '1337',
            TopicArn: fifoConfiguration.TopicsArn[0]
        })
    })

    it('should return true if successful when sending to a fifo sns and a custom MessageGroupId', async () => {
        const fifoConfiguration: ChiselShardedSnsConfiguration<ExampleAggregateRoot> = {
            ...configuration,
            IsFifo: true,
            GetMessageGroupId: (event: InternalTriggeredEvent, state: ExampleAggregateRoot) => state.message
        }

        const event: InternalTriggeredEvent = {
            contextId: '1337',
            eventId: '001',
            eventDate: 9000,
            type: 'SomethingCreated',
            body: {
                message: 'Awesome message'
            }
        }

        const state: ExampleAggregateRoot = {
            message: 'Awesome message'
        }

        const snsClient = {
            send: jest.fn().mockResolvedValue({ MessageId: 'm3554g3'})
        } as any

        const hashCode = jest.fn().mockReturnValue(21)

        const maybeSuccess = await awsActor.publishShardedEvent(hashCode)(snsClient, fifoConfiguration)(event, state)

        expect(maybeSuccess.isPresent).toBeTruthy()
        expect(maybeSuccess.get()).toBeTruthy()

        expect(hashCode).toBeCalledTimes(1)
        expect(hashCode).toBeCalledWith(event.contextId)

        expect(snsClient.send).toBeCalledTimes(1)
        expect(snsClient.send.mock.calls[0][0] instanceof PublishCommand).toBeTruthy()
        expect(snsClient.send.mock.calls[0][0].input).toStrictEqual({
            Message: JSON.stringify({
                event: event,
                state: state
            }),
            MessageAttributes: {
                bc: {
                    DataType: 'String',
                    StringValue: fifoConfiguration.BoundedContext
                },
                eventType: {
                    DataType: 'String',
                    StringValue: event.type.toString()
                }
            },
            MessageGroupId: 'Awesome message',
            TopicArn: fifoConfiguration.TopicsArn[0]
        })
    })

})
