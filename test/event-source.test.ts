import {awsEventSource, ChiselDynamoConfiguration} from '../src'
import {InternalTriggeredEvent} from '@othree.io/chisel/lib'
import {PutItemCommand, QueryCommand} from '@aws-sdk/client-dynamodb'
import {marshall} from '@aws-sdk/util-dynamodb'

describe('event-source', () => {
    const configuration: ChiselDynamoConfiguration = {
        TableName: 'ChiselEvents',
        SnapshotFrequency: 100
    }
    describe('persist', () => {
        it('should persist the specified event', async () => {

            const dynamoDb = {
                send: jest.fn().mockReturnValue(Promise.resolve(true))
            }

            const event: InternalTriggeredEvent = {
                eventId: '001',
                body: {salutation: 'Hello!', something: [], undefinedValue: undefined},
                eventDate: 9000,
                contextId: '1337',
                type: 'SalutationCreated'
            }

            const maybeEvent = await awsEventSource.persist(dynamoDb as any, configuration)(event)

            expect(maybeEvent.isPresent).toBeTruthy()
            expect(maybeEvent.get()).toStrictEqual(event)

            const sendMock = (dynamoDb.send as jest.Mock).mock
            expect(dynamoDb.send).toBeCalledTimes(1)
            expect(dynamoDb.send.mock.calls[0][0] instanceof PutItemCommand).toBeTruthy()
            expect(dynamoDb.send.mock.calls[0][0].input).toStrictEqual({
                Item: marshall(event, {removeUndefinedValues: true}),
                TableName: configuration.TableName
            })
        })
    })

    describe('getEvents', () => {
        it('should get the events for the specified contextId', async () => {
            const expectedEvents = [
                {
                    contextId: '1337',
                    eventId: '003',
                    eventDate: 3,
                    type: 'ValueSubstracted',
                    body: 3
                },
                {
                    contextId: '1337',
                    eventId: '002',
                    eventDate: 2,
                    type: 'ValueAdded',
                    body: 10
                },
                {
                    contextId: '1337',
                    eventId: '001',
                    eventDate: 1,
                    type: 'Initialized',
                    body: 1
                }
            ]
            const dynamoDb = {
                send: jest.fn().mockReturnValue(Promise.resolve({Items: expectedEvents.map(_ => marshall(_))}))
            }

            const maybeEvents = await awsEventSource.getEvents(dynamoDb as any, configuration)('1337')

            expect(maybeEvents.isPresent).toBeTruthy()
            expect(maybeEvents.get()).toStrictEqual(expectedEvents)

            expect(dynamoDb.send).toBeCalledTimes(1)
            expect(dynamoDb.send.mock.calls[0][0] instanceof QueryCommand).toBeTruthy()
            expect(dynamoDb.send.mock.calls[0][0].input).toStrictEqual({
                TableName: configuration.TableName,
                KeyConditionExpression: 'contextId = :contextId',
                ExpressionAttributeValues: {
                    ':contextId': {'S': '1337'}
                },
                Limit: configuration.SnapshotFrequency,
                ScanIndexForward: false
            })
        })

        it('should return an empty array if no events are returned', async () => {
            const dynamoDb = {
                send: jest.fn().mockReturnValue(Promise.resolve({}))
            }

            const maybeEvents = await awsEventSource.getEvents(dynamoDb as any, configuration)('1337')

            expect(maybeEvents.isPresent).toBeTruthy()
            expect(maybeEvents.get()).toStrictEqual([])

            expect(dynamoDb.send).toBeCalledTimes(1)
            expect(dynamoDb.send.mock.calls[0][0] instanceof QueryCommand).toBeTruthy()
            expect(dynamoDb.send.mock.calls[0][0].input).toStrictEqual({
                TableName: configuration.TableName,
                KeyConditionExpression: 'contextId = :contextId',
                ExpressionAttributeValues: {
                    ':contextId': {'S': '1337'}
                },
                Limit: configuration.SnapshotFrequency,
                ScanIndexForward: false
            })
        })
    })
})
