import {handlers} from '../src'
import {CommandResult} from '@othree.io/chisel'
import {Optional} from '@othree.io/optional'

describe('handleLambdaCommand', () => {
    interface Person {
        readonly firstName: string
        readonly lastName: string
    }
    it('should handle a command from a lambda event', async () => {
        const expectedResult: CommandResult<Person> = {
            state: {
                firstName: 'Chuck',
                lastName: 'Norris'
            },
            events: []
        }

        const handle = jest.fn().mockResolvedValue(Optional(expectedResult))

        const lambdaCommand: handlers.LambdaCommand = {
            command: {
                type: 'CreatePerson',
                body: {
                    firstName: 'Chuck',
                    lastName: 'Norris'
                }
            }
        }

        const result = await handlers.handleLambdaCommand(handle)(lambdaCommand)

        expect(result).toStrictEqual(expectedResult)
    })
})
