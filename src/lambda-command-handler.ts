import {actor, Command} from '@othree.io/chisel'

export interface LambdaCommand {
    readonly command: Command
}

export const handleLambdaCommand = <AggregateRoot>(handle: actor.Handle<AggregateRoot>) => async (event: LambdaCommand) => {
    const {command} = event

    return (await handle(command)).get()
}
