import {Optional, TryAsync} from '@othree.io/optional'
import {SNSClient, PublishCommand} from '@aws-sdk/client-sns'
import {InternalTriggeredEvent, actor} from '@othree.io/chisel'
import {BaseChiselSnsConfiguration, ChiselShardedSnsConfiguration, ChiselSnsConfiguration} from './types'
import {MessageAttributeValue} from '@aws-sdk/client-sns/dist-types/models/models_0'

export const hashCode = (value: string) => {
    return [...value].reduce(
        (hash: number, c: string) => (Math.imul(31, hash) + c.charCodeAt(0)) | 0,
        0,
    )
}

const publishEventToTopic = <AggregateRoot>(snsClient: SNSClient, configuration: BaseChiselSnsConfiguration<AggregateRoot>, topicArn: string) => (event: InternalTriggeredEvent, state: AggregateRoot): Promise<Optional<boolean>> => {
    return TryAsync(async () => {
        const metadata = Optional(event.metadata).map<Record<string, MessageAttributeValue>>(metadata => {
            return Object.keys(metadata).reduce((acum, key) => {
                return {
                    ...acum,
                    [key]: {
                        DataType: 'String',
                        StringValue: metadata[key],
                    },
                }
            }, {})
        }).orElse({})
        const command = new PublishCommand({
            Message: JSON.stringify({
                event: event,
                state: state,
            }),
            MessageAttributes: {
                ...metadata,
                bc: {
                    DataType: 'String',
                    StringValue: configuration.BoundedContext,
                },
                eventType: {
                    DataType: 'String',
                    StringValue: event.type.toString(),
                },
            },
            MessageGroupId: configuration.IsFifo ? Optional(configuration.GetMessageGroupId)
                .map(_ => _!(event, state))
                .orElse(event.contextId.toString()) : undefined,
            TopicArn: topicArn,
        })
        return snsClient.send(command)
    }).then(maybeResult => maybeResult.map(_ => true))
}

export const publishShardedEvent = (hashCode: (value: string) => number) =>
    <AggregateRoot>(snsClient: SNSClient, configuration: ChiselShardedSnsConfiguration<AggregateRoot>): actor.PublishEvent<AggregateRoot> =>
        async (event: InternalTriggeredEvent, state: AggregateRoot): Promise<Optional<boolean>> => {
            const topicIdx = hashCode(event.contextId) % configuration.TopicsArn.length

            const topic = configuration.TopicsArn[topicIdx]

            return publishEventToTopic(snsClient, configuration, topic)(event, state)
        }

export const publishEvent = <AggregateRoot>(snsClient: SNSClient, configuration: ChiselSnsConfiguration<AggregateRoot>): actor.PublishEvent<AggregateRoot> =>
    async (event: InternalTriggeredEvent, state: AggregateRoot): Promise<Optional<boolean>> => {
        return publishEventToTopic(snsClient, configuration, configuration.TopicArn)(event, state)
    }
