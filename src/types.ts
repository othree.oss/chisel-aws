import {ChiselEvent} from '@othree.io/chisel'

export interface ChiselDynamoConfiguration {
    readonly TableName: string
    readonly SnapshotFrequency: number
}

export interface BaseChiselSnsConfiguration<AggregateRoot> {
    readonly BoundedContext: string
    readonly IsFifo: boolean
    readonly GetMessageGroupId?: (event: ChiselEvent, state: AggregateRoot) => string
}

export interface ChiselSnsConfiguration<AggregateRoot> extends BaseChiselSnsConfiguration<AggregateRoot> {
    readonly TopicArn: string
}

export interface ChiselShardedSnsConfiguration<AggregateRoot> extends BaseChiselSnsConfiguration<AggregateRoot> {
    readonly TopicsArn: Array<string>
}
