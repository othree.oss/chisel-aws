import {
    DynamoDBClient,
    PutItemCommand,
    QueryCommand
} from '@aws-sdk/client-dynamodb'
import {marshall, unmarshall} from '@aws-sdk/util-dynamodb'
import {InternalTriggeredEvent, eventSource} from '@othree.io/chisel'
import {Optional, TryAsync} from '@othree.io/optional'
import {ChiselDynamoConfiguration} from './types'

export const persist = (dynamoDb: DynamoDBClient, configuration: ChiselDynamoConfiguration): eventSource.PersistEvent =>
    async (event: InternalTriggeredEvent) => {
        return TryAsync(async () => {
            const command = new PutItemCommand({
                Item: marshall(event, {removeUndefinedValues: true}),
                TableName: configuration.TableName
            })

            await dynamoDb.send(command)

            return event
        })
    }

export const getEvents = (dynamoDb: DynamoDBClient, configuration: ChiselDynamoConfiguration) =>
    async (contextId: string): Promise<Optional<Array<InternalTriggeredEvent>>> => {
        return TryAsync(async () => {
            const command = new QueryCommand({
                TableName: configuration.TableName,
                KeyConditionExpression: 'contextId = :contextId',
                ExpressionAttributeValues: {
                    ':contextId': {'S': contextId}
                },
                Limit: configuration.SnapshotFrequency,
                ScanIndexForward: false
            })

            const result = await dynamoDb.send(command)

            if (result.Items) {
                return (result.Items.map(_ => unmarshall(_))) as Array<InternalTriggeredEvent>
            }

            return []
        })
    }
